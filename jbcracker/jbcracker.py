import asyncio
import os
import tempfile

from pyppeteer import launch


class JbCracker:
    """
    Download and install the latest jetbrains-agent.
    """

    async def download(self) -> str:
        """
        Download the latest jetbrains-agent from the shareappscrack.com website.
        """
        browser = await launch(headless=True)
        page = await browser.newPage()
        await page.goto('https://shareappscrack.com/?s=jetbrains+pycharm')

        await page.waitForXPath('//*[@id="main-site"]/div/div[1]/div/div[2]/div/div/div/a')
        elements = await page.xpath('//*[@id="main-site"]/div/div[1]/div/div[2]/div/div/div/a')
        await elements[0].click()

        await page.waitForSelector('.btn-download-sliderbar')
        elements = await page.querySelectorAll('.btn-download-sliderbar')
        await elements[0].click()

        await asyncio.sleep(10)
        elements = await page.xpath('//td[text()[contains(.,"Agent")]]/following-sibling::td/a')
        await elements[0].click()

        await asyncio.sleep(10)
        pages = await browser.pages()
        page = pages[-1]
        elements = await page.xpath('//*[@id="btn-gl-download-recommend"]')
        await elements[0].click()

        await asyncio.sleep(30)
        download_directory = tempfile.mkdtemp()
        await page._client.send('Page.setDownloadBehavior', {'behavior': 'allow', 'downloadPath': download_directory})
        elements = await page.querySelectorAll('#download-link-info-recommend')
        await elements[0].click()
        archive_file = await self._wait_for_download_completion(download_directory)
        await browser.close()

        return archive_file

    async def install(self, plugins_directory: str) -> None:
        """
        Install the latest jetbrains-agent into the specified directory.
        """
        archive_file = await self.download()
        # TODO: Unrar jetbrains-agent-latest.zip from the archives with 'www.ShareAppsCrack.com' as password.
        # TODO: Unzip jetbrains-agent-latest.zip to the plugins_directory.

    @staticmethod
    async def _wait_for_download_completion(directory: str) -> str:
        """
        Wait for browser download completion.
        """
        while len(os.listdir(directory)) == 0:
            await asyncio.sleep(0.5)
        downloaded_file = None
        while not downloaded_file or downloaded_file.endswith('.crdownload'):
            downloaded_file = next(os.scandir(directory)).path
            await asyncio.sleep(0.5)
        return downloaded_file


async def main():
    cracker = JbCracker()
    await cracker.download()


if __name__ == '__main__':
    asyncio.run(main())
